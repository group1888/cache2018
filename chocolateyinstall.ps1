$ErrorActionPreference = 'Stop'

$UNZIP = 'D:\$$ID'

$packageArgs = @{
  packageName   = '$$ID'
  url           = '$$URL'
  unzipLocation = $UNZIP
  checksumType  = 'sha256'
}

Install-ChocolateyZipPackage @packageArgs

&$($UNZIP + '\SYS_CACHE_INSTALL.ps1') $UNZIP


