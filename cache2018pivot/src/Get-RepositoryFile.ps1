# Get-RepositoryFile.ps1 / Get a file from repository / HHLA - Iten / 2020-09-08
# Version 202009-1
#-----------------------------------------------------------------------
#
function Test-DownloadPath {
    param (
        [String] $downLoadURL
    )
    $goodPath = $false
    $goodProtocol = $false
    $protocolArray = $downLoadURL.split(":")
    if ($protocolArray[0] -like 'http*') {
        $goodProtocol = $true
    }
    $fileSpecification = $protocolArray[1]
    $fileArray = $fileSpecification.split("/")
    if ($fileArray.Count -gt 0) {
        $goodPath = $true
    }
    if ($goodProtocol -and $goodPath) {
        $goodPathAndFile = $true
    }
    else {
        $goodPathAndFile = $false
    }
    return $goodPathAndFile
}

$fileName = $args[0]
$destinationPath = $args[1]
$errorFound = $true
if ($fileName -eq $null) {
    Write-Host "Wrong argument specified`n"
    return $errorFound
}
if (Test-DownloadPath -downLoadURL $fileName) {
    #prepare curl (Invoke-WebRequest)
    $AllProtocols = [System.Net.SecurityProtocolType]'Ssl3,Tls,Tls11,Tls12'
    [System.Net.ServicePointManager]::SecurityProtocol = $AllProtocols
    $tmpFileArray = $fileName.split('/')
    $tmpFileArrayLastElement = $tmpFileArray.Count-1
    $DEST_FILE = $destinationPath + '\' + $tmpFileArray[$tmpFileArrayLastElement]
    Write-Host "Downloading $fileName into $DEST_FILE"
    try {
        Invoke-WebRequest -Uri $fileName -OutFile $DEST_FILE
    }
    catch {
        $statusCode = $_.Exception.Response.StatusCode.value__
        if ($statusCode -eq 404) {
            Write-Host -ForegroundColor red "File $fileName not found"
            return $errorFound
        }
    }
    if (Test-Path $DEST_FILE) {
        Write-Host "$DEST_FILE got from repository"
        Write-Host "Unzipping $DEST_FILE"
        Expand-Archive -Path $DEST_FILE -DestinationPath $destinationPath -Force
        Remove-Item -Path $DEST_FILE
        $errorFound = $false
    }
    else {
        Write-Host "Error getting $fileName from repository`n$DEST_FILE doesn't exist"
        $errorFound = $true
    }
}
else {
    Write-Host "Wrong download URL ($downLoadURL) specified"
    $errorFound = $true
}
return $errorFound