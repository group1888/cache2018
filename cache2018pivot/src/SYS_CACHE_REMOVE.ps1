﻿# SYS_CACHE_REMOVE.ps1 / Remove Caché automatically / HHLA - Iten - 2018-06-07
# Version 201908-1
#
# Version 2019-04-1.0
# Starting this script in non-Admin-environment
# Remove local external sources in reference environment
#
# Version 201908-1
# Remove environment variable CACHE_DB out of machine context
#
# Version 202009-1
# Caché installation file in artifactory or on local disk
#
#
#------ FUNCTIONS ----
function modifyEnvVar() {
    $currentPath = $env:Path
    $pathArray = $currentPath.split(';')
    $newPath=""
    for ($i=0; $i -lt $pathArray.Length; $i++) {
        $pathElement = $pathArray[$i]
        if ((-Not $currentPath.contains($pathElement)) -And (-Not($pathElement.contains($INSTALL_DIR)))) {
            if ($pathElement.Length -gt 0) {
                $newPath += "$pathElement;"
            }
        }
    }
    [System.Environment]::SetEnvironmentVariable('PATH',$newPath,'User')
    [System.Environment]::SetEnvironmentVariable('CACHE_DB','','User')  # deprecated. This line will be removed in later versions. Now we keep it in order to be sure that this variable is deleted in installations with old version of installer script
}

function Get-InstallationFile {
    param (
        [String]
        $Path
    )
    $configFile = Join-Path $Path -ChildPath 'config.json'
    if (-not (Test-Path $configFile)) {
        return $null
    }
    $installFiles = Get-Content -raw -path $configFile | ConvertFrom-JSON
    return $installFiles.files.installation
}
#------ SCRIPT AREA ----------------------------------------------------------------------------

$REFERENCE_PATH = $(Get-Location).Path
$REFERENCE_CACHE_PATH = Join-Path -Path $REFERENCE_PATH -ChildPath 'cache'
if (-not (Test-Path $REFERENCE_CACHE_PATH)) {
    Write-Host "Invalid reference environment`nExiting de-installation`n"
    exit 1
}
$INSTALL_EXE = (Get-ChildItem $REFERENCE_CACHE_PATH\cache*win_x64.exe)
if ($INSTALL_EXE -eq $null) {
    $installationFile = Get-InstallationFile -Path $REFERENCE_CACHE_PATH
    if ($installationFile -ne $null) {
        $errorFound = & $REFERENCE_CACHE_PATH\Get-RepositoryFile.ps1 $installationFile $REFERENCE_CACHE_PATH
    }
    else {
        Write-Host "Configuration file not found"
    }
    #later we will download the setup file from artifactory
}

#see SYS_CACHE_INSTALL.ps1 for details about this code
$CACHE_CONFIG = $env:Computername.split('-')[0]

$INSTALL_DIR = "C:\Intersystems\Cache\$CACHE_CONFIG"
$DATA_DIR = "D:\$CACHE_CONFIG"

if (-not (Test-Path $INSTALL_DIR)) {
    Write-Host "No Caché installation found`nExiting script"
    if (Test-Path $DATA_DIR) {
        Write-Host "Old installation data found which will be removed now"
        Remove-Item -Path $DATA_DIR -Recurse
    }
    exit 1
}

Write-Host "Removing Caché-Instance $CACHE_CONFIG"
$currentPath = Get-Location
$tmpCmdFile = "$currentPath`\Remove_$CACHE_CONFIG.ps1"
$null=New-Item $tmpCmdFile -Force  #Force because file might still exist.
Set-Content $tmpCmdFile "Start-Process $INSTALL_DIR\bin\ccontrol.exe -Wait -NoNewWindow -ArgumentList ""stop $CACHE_CONFIG"""
$argList="""/instance $CACHE_CONFIG /qn remove=ALL"""
Add-Content $tmpCmdFile "Start-Process -FilePath $INSTALL_EXE -Wait -NoNewWindow -ArgumentList $argList"
Add-Content $tmpCmdFile "Remove-Item -Recurse -Path $INSTALL_DIR -Force -ErrorAction SilentlyContinue"
Add-Content $tmpCmdFile "Remove-Item -Recurse -Path $DATA_DIR -Force -ErrorAction SilentlyContinue"
Add-Content $tmpCmdFile "[System.Environment]::SetEnvironmentVariable('CACHE_DB','','machine')"
Start-Process -FilePath "PowerShell" -Wait -WindowStyle Minimized -Verb runas -ArgumentList "$tmpCmdFile"
Remove-Item $tmpCmdFile
modifyEnvVar
Remove-Item -Path D:\Referenzumgebung-Cache\ref\cache\externalSources -ErrorAction SilentlyContinue -Recurse
if ((Test-Path $INSTALL_DIR) -or (Test-Path $DATA_DIR)) {
    Write-Host "`nRemoval of Caché-Instance $CACHE_CONFIG failed`n"
    Write-Host "Please reboot this computer and delete following directories manually"
    if (Test-Path $INSTALL_DIR) {
        Write-Host "- $INSTALL_DIR"
        Write-Host "Try to delete this directory now..."
        Remove-Item -Path $INSTALL_DIR -Recurse -ErrorAction SilentlyContinue
        $staExit=1
    }
    if (Test-Path $DATA_DIR) {
        Write-Host "- $DATA_DIR"
        Write-Host "Try to delete this directory now..."
        Remove-Item -Path $DATA_DIR -Recurse -ErrorAction SilentlyContinue
        $staExit=1
    }
}
else {
    $staExit=0
    Write-Host "`nCaché-Instance $CACHE_CONFIG has been removed successfully`n"
    Write-Host -ForegroundColor yellow "You should reboot this computer in order to get rid of environment variable CACHE_DB completely`n"
}
exit $staExit